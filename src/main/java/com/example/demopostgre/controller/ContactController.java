package com.example.demopostgre.controller;


import com.example.demopostgre.entity.Contact;
import com.example.demopostgre.service.ContactService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;

@RestController
@RequestMapping("/")
public class ContactController {

    @Autowired
    ContactService contactService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Contact getOne(@PathVariable ("id") Long id) {

        return (contactService.getOne(id));
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity getAll() {

        return ResponseEntity.ok(contactService.getAll());
    }

    @RequestMapping(method = RequestMethod.POST)
    public Contact create(Contact contact) {
        return contactService.create(contact);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/contacts/{id}")
    public void updateById(@PathVariable long id, @RequestBody @Valid Contact contact) throws NotFoundException {

        contactService.update(contact, id);
    }



}
