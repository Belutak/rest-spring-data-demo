package com.example.demopostgre.service;



import com.example.demopostgre.entity.Contact;
import com.example.demopostgre.exception.MyResourceNotFoundException;
import com.example.demopostgre.repository.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ContactServiceImpl implements ContactService {

    @Autowired
    ContactRepository contactRepository;

    @Override
    public Contact getOne(Long id) {
        return contactRepository.findById(id).get();
    }

    @Override
    public List<Contact> getAll() {
        List<Contact> contactList = (List<Contact>) contactRepository.findAll();
        return contactList;
    }

    @Override
    public Contact create(Contact contact) {
        return contactRepository.save(contact);
    }

    @Override
    public Contact update(Contact contact, long id) {
        Optional<Contact> existing = contactRepository.findById(id);
        if (!existing.isPresent()) {
            throw new MyResourceNotFoundException("Mistake!");
        }
        Contact c = existing.get();
        c.setName(contact.getName());
        c.setAddress(contact.getAddress());
        c.setNumber(contact.getNumber());
        return c;
    }


    @Override
    public void delete(Long id) {
        Optional<Contact> contact = contactRepository.findById(id);
        if(!contact.isPresent()){
            throw new MyResourceNotFoundException("Contact not found for id " + id);
        }
        contactRepository.deleteById(id);
    }
}
