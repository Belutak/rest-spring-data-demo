package com.example.demopostgre.service;



import com.example.demopostgre.entity.Contact;

import java.util.List;
import java.util.Optional;

public interface ContactService {


    Contact getOne(Long id);
    List<Contact> getAll();
    Contact create(Contact contact);
    Contact update(Contact contact, long id);
    void delete(Long id);
}
