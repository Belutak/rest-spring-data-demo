package com.example.demopostgre.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

public final class MyResourceNotFoundException extends RuntimeException {

    public MyResourceNotFoundException() {
        super();
    }

    public MyResourceNotFoundException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public MyResourceNotFoundException(final String message) {
        super(message);
    }

    public MyResourceNotFoundException(final Throwable cause) {
        super(cause);
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    public static class ContactNotFoundException extends RuntimeException {

        public ContactNotFoundException(String exception) {
            super(exception);
        }

    }
}
