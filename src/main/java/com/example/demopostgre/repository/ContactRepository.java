package com.example.demopostgre.repository;

import com.example.demopostgre.entity.Contact;
import org.springframework.data.repository.CrudRepository;

public interface ContactRepository extends CrudRepository<Contact, Long> {

}

