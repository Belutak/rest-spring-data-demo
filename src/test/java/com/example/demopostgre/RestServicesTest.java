package com.example.demopostgre;

import com.example.demopostgre.entity.Contact;
import com.example.demopostgre.repository.ContactRepository;
import com.example.demopostgre.service.ContactServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class RestServicesTest {

//	@Test
//	public void testSomething() {
//		String a = "Nemanjna";
//		assertNotNull(a);
//	}

    @Mock
    private ContactRepository contactRepository;
    @InjectMocks
    private ContactServiceImpl underTest;

    @Test
    public void testGetOne() {
        Long id = 1l;
        Contact expected = new Contact();

        Mockito.when(contactRepository.findById(1L)).thenReturn(Optional.of(expected));

        Contact actual = underTest.getOne(id);

        assertEquals(actual.getId(), expected.getId());
    }

    @Test
    public void testGetAll() {

        List<Contact> contactList = new ArrayList<Contact>();
        Contact contact1 = new Contact(4l, "zoran", "mile", "gogi");
        Contact contact2 = new Contact(11, "Mki", "ggl", "chair");
        Contact contact3 = new Contact(3, "kkd", "kjasldf", "ds");

        contactList.add(contact1);
        contactList.add(contact2);
        contactList.add(contact3);

        Mockito.when(contactRepository.findAll()).thenReturn(contactList);

        List<Contact> actual = underTest.getAll();

        assertTrue(!actual.isEmpty());
        assertEquals(actual.getClass(), contactList.getClass());
        assertEquals(actual.get(1), contactList.get(1));
    }

    @Test
    public void saveTest() {

        Contact expected = new Contact(2l, "test", "me", "again");

        Mockito.when(contactRepository.save(expected)).thenReturn(expected);
        Contact actual = underTest.create(expected);

        assertTrue(actual.equals(expected));
        assertNotNull(actual);
        assertEquals(actual.getClass(), expected.getClass());
        assertEquals(actual.getId(), expected.getId());
        assertEquals(actual, expected);
    }

    @Test
    public void updateTest() {
        //prepare
        Contact contact = new Contact(2l, "test", "me", "again");
        Mockito.when(contactRepository.findById(2l)).thenReturn(Optional.of(contact));
        //exercise
        Contact update = underTest.update(contact, 2l);
        //verify
        assertEquals(update.getName(), "test");
        assertEquals(update.getAddress(), "me");
        assertEquals(update.getNumber(), "again");

    }


    @Test
    public void deleteTest() {

        Long id = 1l ;
        Contact contact = new Contact(id, "test", "me", "again");
        Mockito.when(contactRepository.findById(id)).thenReturn(Optional.of(contact));

        underTest.delete(id);

        verify(contactRepository, times(1)).deleteById(1l);

    }


}

